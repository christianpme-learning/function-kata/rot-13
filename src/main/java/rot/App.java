package rot;

import java.util.ArrayList;
import java.util.List;

public class App 
{
	public String encode(String string) {
		StringBuilder encodedBuider = new StringBuilder();
		for(Character character: string.toCharArray()){
			if(Character.isLetterOrDigit(character)){
				for(Character c : translateUmlauts(Character.toUpperCase(character)).toCharArray()){
					encodedBuider.append(encodeChar(c));
				}
			}
			else {
				encodedBuider.append(character);
			}
		}
		return encodedBuider.toString();
	}

	public String encodeChar(Character character) {
		List<Character> list = generateAlphabetList();
		
		int index = list.indexOf(Character.toUpperCase(character));

		int searchIndex=0;
		if(index+13<list.size()){
			searchIndex=index+13;
		}
		else {
			searchIndex=Math.abs(list.size()-(index+13));
		}

		return list.get(searchIndex).toString();
	}

	public List<Character> generateAlphabetList() {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		ArrayList<Character> list = new ArrayList<Character>();
		for(Character c : alphabet.toCharArray()){
			list.add(c);
		}
		return list;
	}

	public String translateUmlauts(char c) {
		String result="";
		switch(c){
			case 'Ö': result="OE";
				break;
			case 'Ä': result="AE";
				break;
			case 'Ü': result="UE";
				break;
			case 'ß': result="SS";
				break;
			default: result=c+"";
				break;
		}
		return result;
	}
}