package rot;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void encodeTest(){

        App app = new App();
        String actual = app.encode("Hello, World");

        assertTrue("URYYB, JBEYQ".equals(actual));
    }

    @Test
    public void encodeUmlautTest(){

        App app = new App();
        String actual = app.encode("Hellö, World");

        assertTrue("URYYBR, JBEYQ".equals(actual));
    }

    @Test
    public void encodeChar_e_Test(){

        App app = new App();
        String actual = app.encodeChar('e');

        assertTrue("R".equals(actual));
    }

    @Test
    public void encodeChar_W_Test(){

        App app = new App();
        String actual = app.encodeChar('W');

        assertTrue("J".equals(actual));
    }

    @Test
    public void generateAlphabetListTest(){
        
        App app = new App();
        List<Character> actual = app.generateAlphabetList();

        assertTrue(actual.size()==26);
        assertTrue("A".equals(actual.get(0).toString()));
        assertTrue("Z".equals(actual.get(25).toString()));
    }

    @Test
    public void translateUmlaut_Ö_Test(){

        App app = new App();
        String actual = app.translateUmlauts('Ö');

        assertTrue("OE".equals(actual));
    }

    @Test
    public void translateUmlaut_Ä_Test(){

        App app = new App();
        String actual = app.translateUmlauts('Ä');

        assertTrue("AE".equals(actual));
    }

    @Test
    public void translateUmlaut_Ü_Test(){

        App app = new App();
        String actual = app.translateUmlauts('Ü');

        assertTrue("UE".equals(actual));
    }

    @Test
    public void translateUmlaut_ß_Test(){

        App app = new App();
        String actual = app.translateUmlauts('ß');

        assertTrue("SS".equals(actual));
    }

    @Test
    public void translateUmlaut_Whitespace_Test(){

        App app = new App();
        String actual = app.translateUmlauts(' ');

        assertTrue(" ".equals(actual));
    }

    @Test
    public void translateUmlaut_Letter_Test(){

        App app = new App();
        String actual = app.translateUmlauts('w');

        assertTrue("w".equals(actual));
    }
}
