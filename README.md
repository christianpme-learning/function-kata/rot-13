# Function Kata “ROT-13”

https://ccd-school.de/coding-dojo/function-katas/rot-13/

![](sonarqube-result.png)

## Variation #1

> git branch variation1

![](sonarqube-result-v1.png)

## Variation #2

> git branch variation2

![](sonarqube-result-v2.png)